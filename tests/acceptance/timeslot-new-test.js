import { module, test } from 'qunit';
import { visit, currentURL, fillIn, click } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import moment from 'moment';
import { formatTime } from 'peek-schedule/utils/format-time';

module('Acceptance | Timeslots | New', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  test('It should schedule a timeslot', async function(assert) {
    const timeslotAttrs = {
      activityName: 'New activity',
      date: moment().add('days', 2).format('YYYY-MM-DD'),
      startTime: '10:00',
      endTime: '14:00',
      numMaxGuests: 5
    };

    await visit('/new');

    await fillIn('.activity-name', timeslotAttrs.activityName);
    await fillIn('.date', timeslotAttrs.date);
    await fillIn('.start-time', timeslotAttrs.startTime);
    await fillIn('.end-time', timeslotAttrs.endTime);
    await fillIn('.num-max-guests', timeslotAttrs.numMaxGuests);

    await click('.submit');

    const timeslotDom = this.element.querySelector('.timeslot-list__item');

    assert.equal(currentURL(), '/', 'Redirected to list route');

    assert.equal(timeslotDom.querySelector('.activity-name').innerText, timeslotAttrs.activityName);
    assert.equal(timeslotDom.querySelector('.date').innerText, moment(timeslotAttrs.date).format('YYYY-MM-DD'));
    assert.equal(timeslotDom.querySelector('.start-time').innerText, formatTime(timeslotAttrs.startTime));
    assert.equal(timeslotDom.querySelector('.end-time').innerText, formatTime(timeslotAttrs.endTime));
    assert.equal(timeslotDom.querySelector('.num-max-guests').innerText, timeslotAttrs.numMaxGuests);
  });

  test('It should not schedule a invalid timeslot', async function(assert) {
    const timeslotAttrs = {
      activityName: '',
      date: moment().subtract('days', 2).format('YYYY-MM-DD'),
      startTime: '10:00',
      endTime: '09:00',
      numMaxGuests: 5
    };

    await visit('/new');

    await fillIn('.activity-name', timeslotAttrs.activityName);
    await fillIn('.date', timeslotAttrs.date);
    await fillIn('.start-time', formatTime(timeslotAttrs.startTime));
    await fillIn('.end-time', formatTime(timeslotAttrs.endTime));
    await fillIn('.num-max-guests', timeslotAttrs.numMaxGuests);

    await click('.submit');

    assert.equal(currentURL(), '/new', 'Did not redirected');
    assert.equal(this.element.querySelectorAll('.input-error').length, 4);
  });
});
