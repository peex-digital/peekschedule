import { module, test } from 'qunit';
import { visit, currentURL, click } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import moment from 'moment';
import { formatTime } from 'peek-schedule/utils/format-time';

module('Acceptance | Timeslots | List', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  test('It should render the scheduled timeslots', async function(assert) {
    const timeslots = this.server.createList('timeslot', 5);

    await visit('/');

    const timeslotList = this.element.querySelectorAll('.timeslot-list__item');

    const [ timeslotDom1, timeslotDom2, timeslotDom3 ] = timeslotList;

    assert.equal(currentURL(), '/');

    assert.equal(timeslotList.length, 3, 'Total timeslots');

    assert.equal(timeslotDom1.querySelector('.activity-name').innerText, timeslots[2].activityName);
    assert.equal(timeslotDom1.querySelector('.date').innerText, moment(timeslots[2].date).format('YYYY-MM-DD'));
    assert.equal(timeslotDom1.querySelector('.start-time').innerText, formatTime(timeslots[2].startTime));
    assert.equal(timeslotDom1.querySelector('.end-time').innerText, formatTime(timeslots[2].endTime));
    assert.equal(timeslotDom1.querySelector('.num-max-guests').innerText, timeslots[2].numMaxGuests);

    assert.equal(timeslotDom2.querySelector('.activity-name').innerText, timeslots[3].activityName);
    assert.equal(timeslotDom2.querySelector('.date').innerText, moment(timeslots[3].date).format('YYYY-MM-DD'));
    assert.equal(timeslotDom2.querySelector('.start-time').innerText, formatTime(timeslots[3].startTime));
    assert.equal(timeslotDom2.querySelector('.end-time').innerText, formatTime(timeslots[3].endTime));
    assert.equal(timeslotDom2.querySelector('.num-max-guests').innerText, timeslots[3].numMaxGuests);

    assert.equal(timeslotDom3.querySelector('.activity-name').innerText, timeslots[4].activityName);
    assert.equal(timeslotDom3.querySelector('.date').innerText, moment(timeslots[4].date).format('YYYY-MM-DD'));
    assert.equal(timeslotDom3.querySelector('.start-time').innerText, formatTime(timeslots[4].startTime));
    assert.equal(timeslotDom3.querySelector('.end-time').innerText, formatTime(timeslots[4].endTime));
    assert.equal(timeslotDom3.querySelector('.num-max-guests').innerText, timeslots[4].numMaxGuests);

  });

  test('It should cancel the scheduled timeslots', async function(assert) {
    this.server.createList('timeslot', 5);

    await visit('/');

    await click('.cancel-timeslot');

    assert.dom('.timeslot-list__item--canceled').exists();
  });

  test('It should show only timeslost for today', async function(assert) {
    const [ , , timeslot] = this.server.createList('timeslot', 5);

    await visit('/');
    await click('.timeslot__time-window .today');

    const timeslotList = this.element.querySelectorAll('.timeslot-list__item');
    const [ timeslotDom ] = timeslotList;

    assert.equal(timeslotList.length, 1, 'Total timeslots');
    assert.equal(timeslotDom.querySelector('.activity-name').innerText, timeslot.activityName);
  });

  test('It should show only timeslost until today', async function(assert) {
    const [ timeslot1 , timeslot2, timeslot3 ] = this.server.createList('timeslot', 5);

    await visit('/');
    await click('.timeslot__time-window .until-today');

    const timeslotList = this.element.querySelectorAll('.timeslot-list__item');
    const [ timeslotDom1, timeslotDom2 , timeslotDom3] = timeslotList;

    assert.equal(timeslotList.length, 3, 'Total timeslots');

    assert.equal(timeslotDom1.querySelector('.activity-name').innerText, timeslot1.activityName);
    assert.equal(timeslotDom2.querySelector('.activity-name').innerText, timeslot2.activityName);
    assert.equal(timeslotDom3.querySelector('.activity-name').innerText, timeslot3.activityName);

  });
});
