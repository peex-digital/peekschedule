function formatTime(time) {
  let [hours, minutes] = time.split(':');

  let period = (+hours > 12) ? 'PM' : 'AM';
  hours = (+hours > 12) ? hours-12 : hours;

  return `${hours}:${minutes} ${period}`;
}

export { formatTime };
