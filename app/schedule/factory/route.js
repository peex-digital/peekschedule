import Route from '@ember/routing/route';
import moment from 'moment';
import { inject } from '@ember/service';

const activityNames = [
  'Walking Tour',
  'New York City SUV Tour',
  'Bass Lesson',
  'Keys Lesson',
  'Play Time'
];

export default class ScheduleFactoryRoute extends Route{
  @inject flashMessages;

  model() {
    this.createTimeslots();
  }

  async createTimeslots() {
    for (let i = 0; i < 5; i++) {
      const random = Math.floor(Math.random() * 5);

      await this.store.createRecord('timeslot', {
        activityName: activityNames[random],
        date: moment().add('days', random-2),
        startTime: `${random + 10}:00`,
        endTime: `${random + 12}:00`,
        numMaxGuests: random + 4
      }).save();
    }

    this.flashMessages.success('Timeslots succefully generated');

    this.transitionTo('schedule');
  }
}
