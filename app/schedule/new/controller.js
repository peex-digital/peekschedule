import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject } from '@ember/service';

export default class ScheduleNewController extends Controller {
  @inject flashMessages;

  @action
  async schedule(e) {
    e.preventDefault();

    if (this.model.timeslot.validate()) {
      await this.model.timeslot.save();

      this.flashMessages.success('Timeslot succefully scheduled');
      return this.transitionToRoute('schedule.list');
    }
  }
}
