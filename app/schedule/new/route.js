import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { action } from '@ember/object';

export default class ScheduleNewRoute extends Route {
  model() {
    return hash({
      timeslot: this.store.createRecord('timeslot')
    });
  }

  @action
  willTransition() {
    const { timeslot } = this.modelFor('schedule.new');
    timeslot.rollbackAttributes();
  }
}
