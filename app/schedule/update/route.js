import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { action } from '@ember/object';

export default class ScheduleNewRoute extends Route {
  model({timeslot_id}) {
    return hash({
      timeslot: this.store.findRecord('timeslot', timeslot_id)
    });
  }

  @action
  willTransition() {
    const { timeslot } = this.modelFor('schedule.update');
    timeslot.rollbackAttributes();
  }
}
