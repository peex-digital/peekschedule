import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import moment from 'moment';

export default class ScheduleListController extends Controller {
  @tracked filterString;
  @tracked maximumDays = '0~+7';

  @inject flashMessages;

  get timeslots() {
    let timeslots = this.model.timeslots;

    if (this.maximumDays) {
      const { from, to } = this.convertMaximumDays(this.maximumDays);

      timeslots = timeslots.filter((timeslot) => {
        const date = moment(timeslot.date).startOf('day');

        return (!from || from <= date) && (!to || date <= to);
      });
    }

    if (this.filterString) {
      timeslots = timeslots.filter((timeslot) => {
        return (
          timeslot.activityName.toLowerCase().match(this.filterString) ||
          moment(timeslot.date).format('YYYY-MM-DD').match(this.filterString)
        );
      });
    }

    return timeslots;
  }

  @action
  cancel(timeslot, e) {
    e.preventDefault();
    e.stopPropagation();

    timeslot.is_canceled = true;
    timeslot.save();

    this.flashMessages.success('Timeslot succefully canceled');
  }

  convertMaximumDays(days) {
    switch (days) {
      case '~0':
        return {
          to: moment().endOf('day')
        }
      case '0':
        return {
          from: moment().startOf('day'),
          to: moment().endOf('day')
        }
      case '0~+7':
        return {
          from: moment().startOf('day'),
          to: moment().add('days', 7).endOf('day')
        }

    }
  }
}
