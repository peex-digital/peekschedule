import Route from '@ember/routing/route';
import { hash } from 'rsvp';

export default class ScheduleListRoute extends Route {
  model(params) {
    return hash({
      timeslots: this.store.query('timeslot', params)
    });
  }
}
