import Route from '@ember/routing/route';
import { inject } from '@ember/service';

export default class ApplicationRoute extends Route {
  @inject moment;

  beforeModel() {
    this.get('moment').setTimeZone('America/Sao_Paulo');
  }
}
