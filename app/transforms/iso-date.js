import Transform from '@ember-data/serializer/transform';
import moment from 'moment';

export default class TemperatureTransform extends Transform {
  deserialize(serialized) {
    if (serialized) {
      return moment(serialized).toDate();
    }

    return serialized;
  }

  serialize(deserialized) {
    if (deserialized) {
      return moment(deserialized).toISOString();
    }

    return deserialized;
  }
}
