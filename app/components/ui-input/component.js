import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class UiInputComponent extends Component {
  @action
  inputEvent({ target: { value } }) {
    if (this.args.onInput) {
      this.args.onInput(value);
    }
  }
}
