import EmberRouter from '@ember/routing/router';
import config from './config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('schedule', { path: '/' }, function() {
    this.route('list', { path: '/' });
    this.route('new');
    this.route('update', { path: '/:timeslot_id/update'});
    this.route('factory');
  });
});
