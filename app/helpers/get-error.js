import { helper } from '@ember/component/helper';
import { get } from '@ember/object';

export default helper(([ model, attribute ]) => {

  const errors = get(model, 'errors');
  if (errors) {
    const error = errors.findBy('attribute', attribute);

    if (error) {
      if (typeof error.message === 'string') {
        return error.message;
      }

      return error.message[0];
    }
  }

  return '';

});
