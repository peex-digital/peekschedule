import { helper } from '@ember/component/helper';
import { formatTime } from 'peek-schedule/utils/format-time';

export default helper(([ time ]) => {

  return formatTime(time);

});
