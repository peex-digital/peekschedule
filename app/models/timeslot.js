import Model, { attr } from '@ember-data/model';
import Validator from 'ember-model-validator/decorators/model-validator';
import moment from 'moment';

@Validator
export default class TimeslotModel extends Model {
  @attr('string') activityName;
  @attr('iso-date') date;
  @attr('string') startTime;
  @attr('string') endTime;
  @attr('number') numMaxGuests;
  @attr('boolean') is_canceled;

  get sortableStartTime() {
    if (!this.startTime) {
      return 0;
    }

    return +this.startTime.replace(':', '');
  }

  validations = {
    activityName: {
      presence: {
        message: 'Fill the activity name',
      },
    },
    date: {
      presence: {
        message: 'Fill the date',
      },
      custom: {
        validation: function (key, value, model) {
          return (
            moment(model.get('date')).startOf('day') > moment().startOf('day')
          );
        },
        message: 'Select a date in the future',
      },
    },
    startTime: {
      presence: {
        message: 'Fill the starting time',
      },
      custom: {
        validation: function (key, value, model) {
          if (model.get('startTime') && model.get('endTime')) {
            return (
              +model.get('startTime').replace(':', '') <
              +model.get('endTime').replace(':', '')
            );
          }
        },
        message: 'The start time needs to be before the end time',
      },
    },
    endTime: {
      presence: {
        message: 'Fill the ending time',
      }
    },
    numMaxGuests: {
      presence: {
        message: 'Fill the maximum guests',
      },
      numericality: {
        message: 'It need to be a number',
      }
    },
  };
}
