import { Factory } from 'ember-cli-mirage';
import moment from 'moment';

export default Factory.extend({
  activityName(i) {
    return `Activity ${i}`;
  },

  date(i) {
    return moment().add('days', i-2);
  },

  startTime() {
    return '13:00';
  },

  endTime() {
    return '17:00';
  },

  numMaxGuests(i) {
    return i;
  }
});
