export default function() {
  this.namespace = '/';

  this.get('/timeslots');
  this.get('/timeslots/:id');
  this.post('/timeslots');
  this.patch('/timeslots/:id');
  this.delete('/timeslots/:id');
}
