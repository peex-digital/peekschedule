# peek-schedule

First of all, thanks for the opportunity to participate in the Peek test.

## How to access

You can access the project by visiting the following URL

* [Peek Scheduler](https://peek-scheduler.herokuapp.com/)

### Timeslots Generators

You can generate random timeslots by visiting the following URL:

* [Scheduler Factory](https://peek-scheduler.herokuapp.com/factory)

<em>Note: the factory generates timeslots in the past and future. If you generated timeslots and the list is still empty, try change the time window filter</em>

### About the project

This project was built using `Ember.js`. For the localStorage management, the project is using the `ember-local-storage`, which enables the `ember-data` to store on localStorage.

### Running locally

To run this project locally, you simple need to clone, install the dependencies and run:

* `ember s`

The project will be available at [localhost:4200](http://localhost:4200)
